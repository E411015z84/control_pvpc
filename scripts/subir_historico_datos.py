'''
Lee cada fichero del directorio datos y los sube a la BBDD de de Mongo Atlas
'''

import os
import pymongo
import json
from dotenv import load_dotenv
from datetime import date, timedelta

load_dotenv()

MDB_USER=os.getenv('MDB_USER')
MDB_PASSWORD=os.getenv('MDB_PASSWORD')
URI_MONGODB_ATLAS = f"mongodb+srv://{MDB_USER}:{MDB_PASSWORD}@sergivv-cluster0.mavet.mongodb.net/control_PVPC?retryWrites=true&w=majority"

cliente = pymongo.MongoClient(URI_MONGODB_ATLAS)
db = cliente.control_PVPC
col = db.PVPC

fecha_inicial = date(2021, 10, 20)
fecha_final = date(2021, 10, 20) 
#fecha_final = date.today()

for i in range(((fecha_final - fecha_inicial).days)+1):
  print(i)
  fecha = str(fecha_inicial + timedelta(days=i))
  fichero = '../datos/datos_' + fecha + '.json'
  
  with open(fichero) as archivo:
    datos = json.load(archivo)
    
    x = col.insert_one(datos)

cliente.close()
