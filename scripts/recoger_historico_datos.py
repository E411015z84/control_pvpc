'''
Programa para recoger el histórico de precios PVPC de la web ESIOS 
de Red Electrica de España y guardalos en ficheros JSON (un fichero por día).
'''

import json
import requests
from datetime import date, timedelta

REQUEST_URL = 'https://api.esios.ree.es/archives/70/download_json?locale=es&date=:fecha:'

fecha_inicio = date(2021, 10, 20)
fecha_fin = date(2021, 10, 20)
# Periodos hasta 31/05/2021 (3 periodos)
'''periodos = ('GEN', 'NOC', 'VHC')'''
# Periodos a partir del 01/06/2021 (2 peridos)
periodos = ('PCB', 'CYM')
precios = {}

def extraer_datos_web(fecha):
  response_json = requests.get(REQUEST_URL.replace(':fecha:', str(fecha))).text
  return response_json

def extraer_precio_periodo(datos, periodo):
  precios_periodo = {}
  for entrada in datos:
    hora = entrada['Hora'][0:2]
    precio = (float(entrada[periodo].replace(',', '.')))/1000
    precio = float("{0:.5f}".format(precio))
    precios_periodo[hora] = precio
  return precios_periodo

for i in range(((fecha_inicio - fecha_fin).days) + 1):
  fecha = str(fecha_inicio - timedelta(days=i))
  datos_dia = json.loads(extraer_datos_web(fecha))['PVPC']
  for j in range(0, len(periodos)):
    precios[periodos[j]] = extraer_precio_periodo(datos_dia, periodos[j])

  datosJSON = {"Fecha": fecha, "Precios": precios}

  ruta_fichero = '../datos/datos_' + fecha + '.json'
  with open(ruta_fichero, 'w') as fichero:
    fichero.write(json.dumps(datosJSON))
    fichero.close()
