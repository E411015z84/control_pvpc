'''
Programa para recoger datos diarios de precios PVPC de la web ESIOS 
de Red Electrica de España y guardalos en un fichero JSON.
'''

import os
import pymongo
import json
import requests
from dotenv import load_dotenv
from datetime import date, timedelta

load_dotenv()
MDB_USER=os.getenv('MDB_USER')
MDB_PASSWORD=os.getenv('MDB_PASSWORD')
URI_MONGODB_ATLAS = f"mongodb+srv://{MDB_USER}:{MDB_PASSWORD}@sergivv-cluster0.mavet.mongodb.net/control_PVPC?retryWrites=true&w=majority"

REQUEST_URL = 'https://api.esios.ree.es/archives/70/download_json?locale=es&date=:fecha:'

cliente = pymongo.MongoClient(URI_MONGODB_ATLAS)
db = cliente.control_PVPC
col = db.PVPC

fecha = str(date.today() + timedelta(days=1))

# Periodos antes del 01/06/2021
#periodos = ('GEN', 'NOC', 'VHC')
# Periodos a partir del 01/06/2021
periodos = ('PCB', 'CYM')
precios = {}

def extraer_datos_web(fecha):
  response_json = requests.get(REQUEST_URL.replace(':fecha:', str(fecha))).text
  return response_json

def extraer_precio_periodo(datos, periodo):
  precios_periodo = {}
  for entrada in datos:
    hora = entrada['Hora'][0:2]
    precio = (float(entrada[periodo].replace(',', '.')))/1000
    precio = float("{0:.5f}".format(precio))
    precios_periodo[hora] = precio
  return precios_periodo


datos_dia = json.loads(extraer_datos_web(fecha))['PVPC']
for i in range(0, len(periodos)):
  precios[periodos[i]] = extraer_precio_periodo(datos_dia, periodos[i])

  datosJSON = {"Fecha": fecha, "Precios": precios}

  ruta_fichero = '../datos/datos_' + fecha + '.json'
  with open(ruta_fichero, 'w') as fichero:
    fichero.write(json.dumps(datosJSON))
    fichero.close()

x = col.insert_one(datosJSON)
cliente.close()